
# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.1] - 2018-01-17
### Added
- Changelog
- ignore and hidden capabilities to adapter

### Changed
- Renamed library


[Unreleased]: https://github.com/sonic182/sonic182_logger/compare/v0.0.1...HEAD
[0.0.1]: https://github.com/sonic182/sonic182_logger/compare/0b99878545ea2c9b622e7bc5333297bf45b9dc12...v0.0.1
