
[![Build Status](https://travis-ci.org/sonic182/my_logger.svg?branch=master)](https://travis-ci.org/sonic182/my_logger)
[![Coverage Status](https://coveralls.io/repos/github/sonic182/my_logger/badge.svg?branch=master)](https://coveralls.io/github/sonic182/my_logger?branch=master)
# My logger

Custom logger utilities for aiohttp

# Development

Install packages with pip-tools:
```bash
pip install pip-tools
pip-compile
pip-compile dev-requirements.in
pip-sync requirements.txt dev-requirements.txt
```

# Contribute

1. Fork
2. create a branch `feature/your_feature`
3. commit - push - pull request

Thanks :)
