"""Custom logger module."""
import logging
import collections
from uuid import uuid4

FMT = logging.Formatter(
    '{asctime}; LEVEL={levelname}; uuid={uuid}; type={type}; {message}',
    style='{'
)


class SonicAdapter(logging.LoggerAdapter):
    """Handle extra params for logging message."""

    def __init__(self, logger, extra=None, **kwargs):
        """Init log adapter."""
        self.ignore = kwargs.pop('ignore', [])
        self.hidden = kwargs.pop('hidden', [])
        super(SonicAdapter, self).__init__(logger, extra)

    def process(self, msg, kwargs):
        """Proccess message."""
        data, kwargs = self.setup_kwargs_data(msg, kwargs)
        return self._parse_data(data), kwargs

    def setup_kwargs_data(self, msg, kwargs=None):
        """Configure data to be logged."""
        data = kwargs.pop('data', {})
        extra = self.extra or {}
        extra = {**extra, **kwargs.pop('extra', {})}
        extra['uuid'] = extra.get('uuid', uuid4().hex)
        extra['type'] = msg
        kwargs['extra'] = extra
        return collections.OrderedDict(sorted(data.items())), kwargs

    def _parse_data(self, extra, key=''):
        """Append data params recursively.

        TODO: change to secuential implementation.
        """
        res = ''
        if isinstance(extra, dict):
            for _key, value in collections.OrderedDict(extra).items():
                temp_key = '{}{}{}'.format(key, ('.' if key else ''), _key)
                res += self._parse_data(value, temp_key)
        elif isinstance(extra, list):
            for ind, item in enumerate(extra):
                temp_key = '{}{}{}'.format(key, ('.' if key else ''), ind)
                res += self._parse_data(item, temp_key)
        else:
            return res + self.log_data(key, extra)
        return res

    def log_data(self, key, value):
        """Log data folowwing some rules."""
        if key in self.ignore:
            return ''
        elif key in self.hidden:
            return '{}={}'.format(key, ''.rjust(len(value), '*'))
        return '{}={}; '.format(key, value)
