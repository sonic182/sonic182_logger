"""Custom Filters."""
import logging


class LevelFilter(logging.Filter):
    """Custom logger filter."""

    def __init__(self, name='', **kwargs):
        """Initialize MyFilter.

        This filter allows loggers or handlers to log just a specific level.
        kwargs must have levels argument with a list of levels allowed to log.
        """
        self.levels = kwargs.get('levels', [])
        super(LevelFilter, self).__init__(name)

    def filter(self, record):
        """Filter record."""
        if record.levelno in self.levels:
            return True
        return False
