"""Custom Handlers."""
import logging
from sonic182_logger import FMT


class ConsoleHandler(logging.StreamHandler):
    """Custom handler for console."""

    def __init__(self, stream=None, **kwargs):
        """Init object and set level."""
        super(ConsoleHandler, self).__init__(stream)
        self.setLevel(kwargs.pop('level', logging.INFO))
        self.setFormatter(FMT)


class FileHandler(logging.FileHandler):
    """Custom handler for File."""

    def __init__(self, *args, **kwargs):
        """Init object and set level."""
        level = kwargs.pop('level', logging.INFO)
        super(FileHandler, self).__init__(*args, **kwargs)
        self.setLevel(level)
        self.setFormatter(FMT)
