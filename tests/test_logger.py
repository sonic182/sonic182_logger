"""My Logger tests."""
import logging
import collections
import pytest
from sonic182_logger import SonicAdapter
from sonic182_logger.handlers import ConsoleHandler


@pytest.fixture
def logger():
    """Logger for tests."""
    _logger = logging.getLogger()
    _logger.addHandler(ConsoleHandler(None, level=logging.DEBUG))
    return _logger


def test_log_data_parsing(logger):
    """Test log datas not raise erros."""
    data = {
        'a': ['b', 'c'],
        'd': ['e', 'f', 'g']
    }
    ordered = collections.OrderedDict(sorted(data.items()))
    msg = SonicAdapter(logger)._parse_data(ordered)
    assert msg == 'a.0=b; a.1=c; d.0=e; d.1=f; d.2=g; '


def test_log_data(logger):
    """Test ignore and hidden log info."""
    key = 'password'
    value = 'secret'
    ignore = ['password']
    assert SonicAdapter(logger, ignore=ignore).log_data(key, value) == ''

    key = 'password'
    value = 'secret'
    hidden = ['password']
    assert SonicAdapter(
        logger, hidden=hidden).log_data(key, value) == '{}=******'.format(key)


def test_setup_kwargs(logger):
    extra = {'uuid': '11223344', 'type': ''}
    logger = SonicAdapter(logger, extra)
    _, kwargs= logger.setup_kwargs_data('', {})
    assert kwargs == {'extra': extra}

